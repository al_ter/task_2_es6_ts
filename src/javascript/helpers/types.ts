export interface IAttributes {
  [key: string]: string;
}

export interface ICallback {
  (): void;
}

export interface IModalData {
  title: string;
  bodyElement: HTMLElement;
  onClose?: ICallback;
}

export interface IElementData {
  readonly tagName: string;
  readonly className?: string;
  readonly attributes?: IAttributes;
}

export interface IFighter {
  readonly _id: number | string;
  readonly name: string;
  readonly source: string;
}

export interface IFighterDetails extends IFighter {
  readonly health: number;
  readonly attack: number;
  readonly defense: number;
}

export interface IFighterCurrent extends IFighterDetails {
  currentHealth: number;
}

export interface ILogItem {
  fighter1Shot: number;
  fighter2Shot: number;
  fighter1Health: number;
  fighter2Health: number;
}

export interface IFight {
  fighter1: number | string;
  fighter2: number | string;
  log: ILogItem[];
}

export type KeyMap = Map<string, boolean>;

export type Combo = [string, string, string];

export type Position = 'left' | 'right';

export interface IFighterSelector {
  (event: Event, fighterId: number | string): void;
}
