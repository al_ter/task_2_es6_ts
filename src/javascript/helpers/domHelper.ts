import { IElementData } from './types';

export function createElement(data: IElementData): HTMLElement {
  const { tagName, className, attributes } = data;
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if (attributes) {
    Object.entries(attributes).forEach(([key, value]) => element.setAttribute(key, value));
  }

  return element;
}
