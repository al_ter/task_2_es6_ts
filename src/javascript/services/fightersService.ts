import { callApi } from '../helpers/apiHelper';
import { IFighterDetails, IFighter, IFighterCurrent, IFight } from '../helpers/types';

class FighterService {
  async getFighters(): Promise<IFighter[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult as IFighter[];
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: number | string): Promise<IFighterDetails> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult as IFighterDetails;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
