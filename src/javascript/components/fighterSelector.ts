import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFighterDetails, ICallback, IFighterSelector } from '../helpers/types';

export function createFightersSelector(): IFighterSelector {
  let selectedFighters: IFighterDetails[] = [];

  return async (event: Event, fighterId: number | string): Promise<void> => {
    const fighter: IFighterDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap: Map<number | string, IFighterDetails> = new Map();

export async function getFighterInfo(fighterId: string | number): Promise<IFighterDetails> {
  if(!fighterDetailsMap.has(fighterId)) {
    try {
      const fighter: IFighterDetails = await fighterService.getFighterDetails(fighterId);
      fighterDetailsMap.set(fighterId, fighter);
    } catch (error) {
      throw error;
    }
  }

  return fighterDetailsMap.get(fighterId) as IFighterDetails;
}

function renderSelectedFighters(selectedFighters: IFighterDetails[]): void {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  if (!fightersPreview) {
    throw new Error('No preview container found');
  }

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighterDetails[]): HTMLElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick: ICallback = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  }) as HTMLImageElement;
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFighterDetails[]): void {
  renderArena(selectedFighters);
}
