import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';
import { IFighterDetails } from '../../helpers/types';

export function showWinnerModal(fighter: IFighterDetails) {
  const fighterImage = createFighterImage(fighter);
  const onClose = () => document.location.reload();

  showModal({
    title: `${fighter.name} WINS!`,
    bodyElement: fighterImage,
    onClose
  });
}
