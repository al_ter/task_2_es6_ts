import { controls, IControls } from '../../constants/controls';
import { IFighterDetails, IFighterCurrent, IFight, ILogItem, KeyMap } from '../helpers/types';

export async function fight([firstFighter, secondFighter]: IFighterDetails[]): Promise<IFighterCurrent> {
  const fighter1: IFighterCurrent = Object.assign(firstFighter, { currentHealth: firstFighter.health });
  const fighter2: IFighterCurrent = Object.assign(secondFighter, { currentHealth: secondFighter.health });
  const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
  const secondFighterHealthBar = document.getElementById('right-fighter-indicator');

  if (!firstFighterHealthBar || !secondFighterHealthBar) {
    throw new Error("No healthbar element found!");
  }

  const CRITICAL_HIT_DELAY: number = 1000; // 10s
  let firstFighterCanCrit: boolean = true;
  let secondFighterCanCrit: boolean = true;

  let fight: IFight = {
    fighter1: fighter1._id,
    fighter2: fighter2._id,
    log: [
    // {
    //     "fighter1Shot": 0,
    //     "fighter2Shot": 0,
    //     "fighter1Health": 0,
    //     "fighter2Health": 0
    // }
    ]
  };

  const pressedKeysMap: KeyMap = setKeysMap(controls);

  return new Promise(resolve => {

    const update = (defender: IFighterCurrent, damage: number): void => {debugger
      const attacker: IFighterCurrent = defender === fighter1 ? fighter2 : fighter1;
      const healthElement: HTMLElement = defender === fighter1 ? firstFighterHealthBar : secondFighterHealthBar;
      defender.currentHealth = Math.max(0, defender.currentHealth - damage);
      healthElement.style.width = `${defender.currentHealth / defender.health * 100}%`;
      
      fight.log.push({
        fighter1Shot: attacker === fighter1 ? damage : 0,
        fighter2Shot: attacker === fighter2 ? damage : 0,
        fighter1Health: fighter1.currentHealth,
        fighter2Health: fighter2.currentHealth
      });

      if (defender.currentHealth === 0) {
        document.removeEventListener('keydown', keydownHandler, false);
        document.removeEventListener('keyup', keyupHandler, false);
        resolve(attacker);
      }
    }

    const fightController = (keysMap: KeyMap): void => {
      if (isFirstFighterCrit(keysMap, firstFighterCanCrit)) {
        update(fighter2, getDamage(fighter1, true, fighter2));
        firstFighterCanCrit = false;
        setTimeout(() => firstFighterCanCrit = true, CRITICAL_HIT_DELAY);
      } else if (isSecondFighterCrit(keysMap, secondFighterCanCrit)) {
        update(fighter1, getDamage(fighter2, true, fighter1));
        secondFighterCanCrit = false;
        setTimeout(() => secondFighterCanCrit = true, CRITICAL_HIT_DELAY);
      } else if (isFirstHitSecondDefend(keysMap)) {
        update(fighter2, 0);
      } else if (isSecondHitFirstDefend(keysMap)) {
        update(fighter1, 0);
      } else if (isFirstFreeHit(keysMap)) {
        update(fighter2, getDamage(fighter1, false, fighter2));
      } else if (isSecondFreeHit(keysMap)) {
        update(fighter1, getDamage(fighter2, false, fighter1));
      }
    }

    const keydownHandler = (e: KeyboardEvent): undefined => {
      if (!pressedKeysMap.has(e.code) || e.repeat) {
        return;
      }
      pressedKeysMap.set(e.code, true);
      fightController(pressedKeysMap);
    }

    const keyupHandler = ({ code }: KeyboardEvent): void => {
      pressedKeysMap.set(code, false);
    };

    document.addEventListener('keydown', keydownHandler, false);
    document.addEventListener('keyup', keyupHandler, false);
  });
}

const setKeysMap = (keyControls: IControls): KeyMap => {
  const result: KeyMap = new Map();

  Object.values(keyControls)
  .map(value => {
    if (value instanceof Array) {
      value.map(innerValue => result.set(innerValue, false));
      return false;
    }
    result.set(value, false);
  });

  return result;
}

export function getDamage(attacker: IFighterCurrent, isCrit: boolean = false, defender?: IFighterCurrent): number {
  let damage: number = 0;

  if (isCrit) {
    damage = getHitPower(attacker, true);
  } else if (!defender) {
    damage = getHitPower(attacker);
  } else {
    damage = getHitPower(attacker) - getBlockPower(defender);
  }
  console.log(`DAMAGE: ${Math.max(0, damage)}`);
  return Math.max(0, damage);
}

export function getHitPower(fighter: IFighterCurrent, isCrit: boolean = false): number {
  const { name, attack } = fighter;
  const criticalHitChance: number = getRandom(1, 2);
  const hitPower = isCrit ? attack * 2 : attack * criticalHitChance;
  console.log(`${name} hits: ${hitPower}`);
  return hitPower;
}

export function getBlockPower(fighter: IFighterCurrent): number {
  const { name, defense } = fighter;
  const dodgeChance: number = getRandom(1, 2);
  const blockPower = defense * dodgeChance;
  console.log(`${name} blocks: ${blockPower}`);
  return blockPower;
}

const getRandom = (min: number, max: number): number => (Math.random() * (max - min) + min);

function isFirstFighterCrit(keysMap: KeyMap, canCrit: boolean): boolean {
  const result: boolean | undefined = canCrit &&
    keysMap.get(controls.PlayerOneCriticalHitCombination[0]) &&
    keysMap.get(controls.PlayerOneCriticalHitCombination[1]) &&
    keysMap.get(controls.PlayerOneCriticalHitCombination[2]);
  return !!result;
}

function isSecondFighterCrit(keysMap: KeyMap, canCrit: boolean): boolean {
  const result: boolean | undefined = canCrit &&
    keysMap.get(controls.PlayerTwoCriticalHitCombination[0]) &&
    keysMap.get(controls.PlayerTwoCriticalHitCombination[1]) &&
    keysMap.get(controls.PlayerTwoCriticalHitCombination[2]);
  return !!result;
}

function isFirstHitSecondDefend(keysMap: KeyMap): boolean {
  const result: boolean | undefined = keysMap.get(controls.PlayerOneAttack) &&
    !keysMap.get(controls.PlayerOneBlock) &&
    keysMap.get(controls.PlayerTwoBlock);
  return !!result;
}

function isSecondHitFirstDefend(keysMap: KeyMap): boolean {
  const result: boolean | undefined = keysMap.get(controls.PlayerTwoAttack) &&
    !keysMap.get(controls.PlayerTwoBlock) &&
    keysMap.get(controls.PlayerOneBlock);
  return !!result;
}

function isFirstFreeHit(keysMap: KeyMap): boolean {
  const result: boolean | undefined = keysMap.get(controls.PlayerOneAttack) &&
    !keysMap.get(controls.PlayerOneBlock);
  return !!result;
}

function isSecondFreeHit(keysMap: KeyMap): boolean {
  const result: boolean | undefined = keysMap.get(controls.PlayerTwoAttack) &&
    !keysMap.get(controls.PlayerTwoBlock);
  return !!result;
}
