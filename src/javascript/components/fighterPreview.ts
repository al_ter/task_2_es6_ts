import { createElement } from '../helpers/domHelper';
import { fightersDetails } from '../helpers/mockData';
import { IFighter, IFighterDetails, Position } from '../helpers/types';

export function createFighterPreview(fighter: IFighterDetails, position: Position): HTMLElement {  
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImage = createFighterImage(fighter);
  const fighterInfo = createFighterInfo(fighter);

  fighterElement.append(fighterImage, fighterInfo);

  return fighterElement;
}

export const createFighterInfo = (fighter: IFighterDetails): HTMLElement => {
  const infoAttributes = [
    'name',
    'health',
    'attack',
    'defense'
  ];

  const fighterInfoElement = createElement({
    tagName: 'div',
    className: `fighter-preview___info`
  });

  let fighterInfo: string = '';

  Object.entries(fighter)
    .map(([key, value]) => {
      fighterInfo += infoAttributes.includes(key)
        ? `<span><strong>${key}</strong>: ${value}</span>`
        : ''
    });

  fighterInfoElement.innerHTML = fighterInfo;
  
  return fighterInfoElement;
}

export function createFighterImage(fighter: IFighterDetails): HTMLImageElement {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  }) as HTMLImageElement;

  return imgElement;
}
