import { Combo } from '../javascript/helpers/types';

export interface IControls {
  PlayerOneAttack: string;
  PlayerOneBlock: string;
  PlayerTwoAttack: string;
  PlayerTwoBlock: string;
  PlayerOneCriticalHitCombination: Combo;
  PlayerTwoCriticalHitCombination: Combo;
}

export const controls: IControls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
}
